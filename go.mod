module gitlab.com/livesocket/pi

go 1.12

require (
	github.com/gammazero/nexus/v3 v3.0.0
	golang.org/x/crypto v0.0.0-20191011191535-87dc89f01550 // indirect
)
