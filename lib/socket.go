package lib

import (
	"context"
	"fmt"
	"log"
	"os"
	"time"

	"github.com/gammazero/nexus/v3/client"
	"github.com/gammazero/nexus/v3/wamp"
	"github.com/gammazero/nexus/v3/wamp/crsign"
)

// Socket a WAMP websocket connection
type Socket struct {
	*client.Client
}

// CallHandler the standard CALL handler function
type CallHandler func(invocation *wamp.Invocation) client.InvokeResult

// Action defines an action for registration with the socket
type Action struct {
	Proc    string
	Handler CallHandler
}

// Subscription defines a subscription for registration with the socket
type Subscription struct {
	Topic   string
	Handler client.EventHandler
}

// NewStandardSocket Returns a new socket connection using the standard livesocket environment variables
// Also auto registers the provided actions and subscriptions
func NewStandardSocket(actions []Action, subscriptions []Subscription) (*Socket, error) {
	socket, err := NewSocket(os.Getenv("NEXUS_ADDRESS"), os.Getenv("NEXUS_REALM"), os.Getenv("NEXUS_USER"), os.Getenv("NEXUS_PWORD"))
	if err != nil {
		return nil, err
	}

	if actions != nil {
		for _, action := range actions {
			socket.Register(action.Proc, action.Handler)
		}
	}

	if subscriptions != nil {
		for _, sub := range subscriptions {
			socket.Subscribe(sub.Topic, sub.Handler)
		}
	}

	go autoPing(socket)

	return socket, nil
}

// NewTestSocket Returns a new test socket connection for functional testing
func NewTestSocket() (*Socket, error) {
	return NewSocket("ws://localhost:9000", "realm1", "1", "test")
}

// NewSocket Returns a new socket connection using the provided parameters
func NewSocket(address string, realm string, user string, password string) (*Socket, error) {
	cfg := client.Config{
		Realm: realm,
		HelloDetails: wamp.Dict{
			"authid": user,
		},
		AuthHandlers: map[string]client.AuthFunc{
			"ticket": func(challenge *wamp.Challenge) (string, wamp.Dict) {
				return password, wamp.Dict{}
			},
		},
		Logger: log.New(os.Stdout, "", 0),
	}

	println("Attempting to connect to WebSocket router...")
	c, err := client.ConnectNet(context.Background(), address, cfg)
	retries := 0
	for err != nil && retries < 30 {
		time.Sleep(2 * time.Second)
		println("Retrying to connect to WebSocket router...")
		c, err = client.ConnectNet(context.Background(), address, cfg)
		if err != nil {
			log.Print(err)
		}
		retries++
	}
	return &Socket{Client: c}, err
}

// SimpleCall Sends a CALL message using the provided parameters
func (socket *Socket) SimpleCall(proc string, args wamp.List, kwargs wamp.Dict) (*wamp.Result, error) {
	return socket.Call(context.Background(), proc, wamp.Dict{wamp.OptInvoke: wamp.InvokeRoundRobin, wamp.OptTimeout: 2000}, args, kwargs, nil)
}

// Register registers a new wamp endpoint using the provided parameters
func (socket *Socket) Register(proc string, handler CallHandler) {
	fn := func(ctx context.Context, invocation *wamp.Invocation) client.InvokeResult {
		result := handler(invocation)
		return result
	}
	println("Registering:", proc)
	socket.Client.Register(proc, client.InvocationHandler(fn), wamp.Dict{wamp.OptDiscloseCaller: true, wamp.OptInvoke: wamp.InvokeRoundRobin})
}

// Subscribe subscribes to a topic using the provided parameters
func (socket *Socket) Subscribe(topic string, handler client.EventHandler) {
	println("Subscribing to:", topic)
	socket.Client.Subscribe(topic, handler, nil)
}

// ErrorResult Helper for returning errors from a CALL handler
func ErrorResult(err interface{}) client.InvokeResult {
	return client.InvokeResult{
		Err: wamp.URI(fmt.Sprint(err)),
	}
}

// ArgsResult Helper for returning args from a CALL handler
func ArgsResult(args ...interface{}) client.InvokeResult {
	list := wamp.List{}
	for _, item := range args {
		list = append(list, item)
	}
	return client.InvokeResult{
		Args: list,
	}
}

func wampCraAuth(secret string) client.AuthFunc {
	return func(c *wamp.Challenge) (string, wamp.Dict) {
		return crsign.RespondChallenge(secret, c, nil), wamp.Dict{}
	}
}

func autoPing(socket *Socket) {
	for true {
		_, err := socket.SimpleCall("public.ping", nil, nil)
		if err != nil {
			fmt.Println(err)
		}
		time.Sleep(30 * time.Second)
	}
}
