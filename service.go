package pi

import (
	"gitlab.com/livesocket/pi/lib"
)

func NewPiService(id string, actions []lib.Action, subscriptions []lib.Subscription) (func() error, *lib.Socket, error) {
	socket, err := lib.NewStandardSocket(actions, subscriptions)
	if err != nil {
		return nil, nil, err
	}

	return func() error {
		err := socket.Close()
		return err
	}, socket, nil
}
